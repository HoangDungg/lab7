﻿using QuanLySach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QuanLySach.Controllers
{
    public class BooksController : ApiController
    {
        Book[] books = new Book[]
        {
            new Book{ Id = 1, Title = "Tôi thấy hoa vàng trên cỏ xanh", AuthorName = "Nguyễn Nhật Ánh", Price = 1, Content = "Truyện kể về tuổi thơ..."},
            new Book { Id = 2, Title = "Pro ASP.NET MVC 5", AuthorName = "Adam Freeman ", Price =3.75M, Content ="The ASP.NET MVC 5 Framework is the lastest evolution of Microsoft's ASP.NET web Platform."}
        };

        public IEnumerable<Book> GetAll()
        {
            return books;
        }
        public IHttpActionResult GetBook (int id)
        {
            var book = books.FirstOrDefault(p => p.Id == id);
            if(book == null)
            {
                return NotFound();
            }
            return Ok(book);
        }
    }
}
